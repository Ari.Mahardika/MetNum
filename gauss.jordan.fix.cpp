/*
* Copyright 2016 Ari Mahardika
* Email : arimahardika.an@gmail.com
* Git   : https://gitlab.com/u/Ari.Mahardika
*/

#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	int n;
	float A[100][101], c, sum=0.0, x[100];
	
	//input array ----------------------------	
	cout<<"===== Input ====="<<endl;
	cout<<"Masukan ukuran matrix = ";
	cin>>n;
	cout<<"Masukkan angka matrix = "<<endl;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n+1;j++){
			cout<<"A[" << i << "][" << j <<"] : ";
			cin>>A[i][j];
		}
	}
	
	//cetak ----------------------------------
	cout<<endl<<"===== Cetak ====="<<endl<<endl;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n+1;j++){
			cout<<setw(5)<<A[i][j]<<" ";			
		}
		cout<<endl;
	}
	cout<<endl;
	
	//proses
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			if(j!=i){	
				c=A[j][i]/A[i][i];
				for(int k=1;k<=n+1;k++){
					A[j][k]=A[j][k]-c*A[i][k];
				}
			}
		}
	}
	
	//cetak ----------------------------------
	cout<<endl<<"===== Cetak ====="<<endl<<endl;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n+1;j++){
			cout<<setw(5)<<A[i][j]<<" ";			
		}
		cout<<endl;
	}
	cout<<endl;
	
	//membuat diagonal 1 ----------------------
	for(int i=n;i>=1;i--){
		if(A[i][i]!=1){
			c=1/A[i][i];
			for(int k=1; k<=n+1; k++){
				A[i][k]=A[i][k] * c;
			}
		}
	}
	//membuat diagonal 1 ----------------------

	//cetak ----------------------------------
	cout<<endl<<"===== Cetak ====="<<endl<<endl;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n+1;j++){
			cout<<setw(5)<<A[i][j]<<" ";			
		}
		cout<<endl;
	}
	cout<<endl;
	
	for(int i=n;i>=1;i--){
		for(int j=n;j>=1;j--){
			if(j!=i){
				c=A[j][i]/A[i][i];
				for(int k=1;k<=n+1;k++){
				A[j][k]=A[j][k]-c*A[i][k];
				}
			}
		}
	}
	
	//cetak ----------------------------------
	cout<<endl<<"===== Cetak ====="<<endl<<endl;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n+1;j++){
			cout<<setw(5)<<A[i][j]<<" ";			
		}
		cout<<endl;
	}
	cout<<endl;
	
return 0;
}

