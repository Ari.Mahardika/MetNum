/*
* Copyright 2016 Ari Mahardika
* Email : arimahardika.an@gmail.com
* Git   : https://gitlab.com/u/Ari.Mahardika
*/
#include <iostream>
#include <iomanip>

using namespace std;

	float a, b, y1,y2, interval, L, l_total=0.0, t;
	int loop;
	int static width = 10;
	
void fungsi_x(float arg1){
	float y = 2 * arg1;
}
int main(){
	//trapezoida
	cout<<"Program Menghitung Integral, metode Riemann \n";
	cout<<"F(x) = 2x\n";
	cout<<"============================================\n";
	cout<<"Masukkan nilai awal   : ";
	cin>>a;
	cout<<"Masukkan nilai akhir  : ";
	cin>>b;
	cout<<"Masukkan jumlah loop yang diinginkan : ";
	cin>>loop;
	
	interval = (b-a)/loop;
	
	cout<<"Interval : "<<interval<<endl<<endl;
	
	y1=2*a;
	y2=2*b;
	t = (y1+y2)/2;
	L = (b-a)*t;
	
	cout<<"Hasil : "<<L<<endl;
	
return 0;
}

