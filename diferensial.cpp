/*
* Author : Ari Mahardika
* Email  : arimahardika.an@gmail.com
* Git    : https://gitlab.com/u/Ari.Mahardika
* Copyright 2016
*/
#include <iostream>
#include <iomanip>

using namespace std;

//(f(x+h)-f(x))/h

float x, h, fxh, fx, y, a;

int main(){
	//diferensial
	cout<<"Diferensial\n";
	cout<<"Rumus yang digunakan : x^3\n";
	cout<<"===========================\n";
	cout<<"Masukkan nilai x : ";
	cin>>x;
	cout<<"Masukkan nilai h : ";
	cin>>h;
	
	fxh = x+h;
	fx = x;
	
	y = ((fxh*fxh*fxh)-(fx*fx*fx))/h;
	
	cout<<"\nMetode Numerik \n";
	cout<<"x      : "<<x<<endl;
	cout<<"h      : "<<h<<endl;
	cout<<"f(x+h) : "<<fxh<<endl;
	cout<<"f(x)   : "<<fx<<endl;
	cout<<"Hasil  : "<<y<<endl;
	
	a = 3*x*x;
	cout<<"\nMetode Analitik \n";
	cout<<"Hasil  : "<<a<<endl;
	
	cout<<"\nError  : "<<y-a<<endl;

	
return 0;
}

