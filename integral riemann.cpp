/*
* Author : Ari Mahardika
* Email  : arimahardika.an@gmail.com
* Git    : https://gitlab.com/u/Ari.Mahardika
* Copyright 2016
*/
#include <iostream>
#include <iomanip>

using namespace std;

	float a, b, y, interval, L, l_total=0.0;
	int loop;
	int number=0;
	int static width = 10;
	
int main(){
	//Integral
	cout<<"Program Menghitung Integral, metode Riemann \n";
	cout<<"F(x) = 2x\n";
	cout<<"============================================\n";
	cout<<"Masukkan nilai awal   : ";
	cin>>a;
	cout<<"Masukkan nilai akhir  : ";
	cin>>b;
	cout<<"Masukkan jumlah loop yang diinginkan : ";
	cin>>loop;
	
	interval = (b-a)/loop;
	
	cout<<"Interval : "<<interval<<endl<<endl;
	
	cout<<setw(5)<<"No"<<setw(width)<<"i"<<setw(width)<<"y"<<setw(width)<<"L"<<setw(width)<<"L_TOTAL"<<endl;
	
	for(float i=a; i<=b; i=i+interval){
		number++;
		y = 2 * i;
		L = interval * y;
		if(i!=1){
			l_total = l_total + L;
		}
		cout<<setw(5)<<number-1<<setw(width)<<i<<setw(width)<<y<<setw(width)<<L<<setw(width)<<l_total<<endl;
	}
return 0;
}

